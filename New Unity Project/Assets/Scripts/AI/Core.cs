﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Core : MonoBehaviour {

    private struct TurnInformation
    {
        public Quaternion Origin;
        public Quaternion Destination;
        public float Time;

        public void Clear()
        {
            Origin = Quaternion.identity;
            Destination = Quaternion.identity;
            Time = 0;
        }
    }

    public float moveSpeed = 3f;
    public float actionRange = 5f;

    private TurnInformation _turnInfo;
    public bool _turn = false;
    public float velocity;
    private float tim = 0;
    private Rigidbody body;
    private List<Transform> Path = new List<Transform>();
    
	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        this.velocity = body.velocity.magnitude;
	}

    private void FixedUpdate()
    {
        if (_turn)
        {
            
            tim += Time.fixedDeltaTime * (1f / _turnInfo.Time);
            transform.transform.rotation = Quaternion.Slerp(_turnInfo.Origin, _turnInfo.Destination, tim);
            if (tim > 1)
            {
                _turn = false;
                _turnInfo.Clear();

            }
        }
        body.AddForce(transform.forward * moveSpeed);

        RaycastHit hitinfo;
        if (Physics.Raycast(new Ray(transform.position, transform.forward), out hitinfo))
        {
            if(Vector3.Distance(hitinfo.collider.transform.position, transform.position) < 2.5f)
            {

                _turnInfo.Origin = transform.rotation;
                _turnInfo.Destination = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90, transform.rotation.eulerAngles.z);

                _turnInfo.Time = body.velocity.magnitude / Vector3.Distance(hitinfo.collider.transform.position, transform.position);
                _turn = true;
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (Application.isPlaying)
        {
            Gizmos.color = Color.black;
            Gizmos.DrawRay(new Ray(transform.position, body.velocity));
        }
    }
}
