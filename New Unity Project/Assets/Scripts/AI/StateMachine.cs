﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{

    internal enum StateMachineState : ushort
    {
        Alert,
        Idle
    }

    public interface IStateMachine
    {
        string Name { get; }

        void UpdateState();
    }

    internal class DefaultStateMachine : IStateMachine
    {
        private string _name;

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public void UpdateState()
        {
            throw new NotImplementedException();
        }
    }

    public class UpperStateMachine
    {
        public List<IStateMachine> StateMachines = new List<IStateMachine>();
        private ushort _index;

        public UpperStateMachine(IStateMachine state)
        {
            _index = 1;

            if (state != null) {
                StateMachines.Add(state);
            }
        }

        public void AddSubStateMachine(IStateMachine machine)
        {
            StateMachines.Add(machine);
        }

        public void UpdateMachine()
        {

        }

    }
}