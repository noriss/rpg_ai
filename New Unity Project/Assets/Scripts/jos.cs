﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jos : MonoBehaviour {

    public GameObject cylinder;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(longer());
        Debug.Log(50 / 60 % 2 == 0);
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator longer()
    {
        for (int i = 0; i < 15; i++)
        {

            cylinder.transform.localScale = new Vector3(cylinder.transform.localScale.x, cylinder.transform.localScale.y * 1.5f, cylinder.transform.localScale.z);
            Matrix4x4 aa = cylinder.transform.worldToLocalMatrix;
            
            Quaternion temps = new Quaternion(cylinder.transform.localRotation.eulerAngles.x, cylinder.transform.localRotation.eulerAngles.y + 90, cylinder.transform.localRotation.z, cylinder.transform.localRotation.w);
            
            cylinder.transform.rotation = temps;
            yield return new WaitForSeconds(1);
        }
    }
}
