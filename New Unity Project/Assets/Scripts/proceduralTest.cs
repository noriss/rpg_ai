﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class proceduralTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(longer());
	}
	
	// Update is called once per frame
	void Update () {

	}

    IEnumerator longer()
    {
        for(int i = 0; i < 15; i++)
        {

            gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x , gameObject.transform.localScale.y * 1.5f, gameObject.transform.localScale.y);
            Quaternion temps = new Quaternion(gameObject.transform.localRotation.eulerAngles.x, gameObject.transform.localRotation.eulerAngles.y + 5, gameObject.transform.localRotation.z, gameObject.transform.localRotation.w);
            gameObject.transform.localRotation = temps;
            yield return new WaitForSeconds(1);
        }
    }
}
